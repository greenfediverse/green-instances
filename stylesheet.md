<!DOCTYPE html>

<html>
<head>
    <meta charset='utf-8' />
    <link rel="stylesheet" media="all" href="style.css" />
</head>
<body>

<p><strong>Hosting provider:</strong></p>

<p>name (+link to website)</p>

<p><strong>Transparency: Public evidence & transparent measurement:</strong></p>

<p>public energy certificate (link), published GHG Protocol (with scope 1-3 emissions) (link), othere evidence of measurements (link)

<p>other information</p>

<p><strong>Public information about the data center in use (+ colocation):</strong></p>

<p>(short summary + source link, for example: https://website_hosting_provider/datacenters)</p>

<p><strong>Public information on the use of renewable energy:</strong></p>

<p>(short summary + source link, for example: https://website_hosting_provider/sustainability)</p>

<p><strong>Other critical notes:</strong></p>

<p>(short summary + source link</p>

</body>
</html>
