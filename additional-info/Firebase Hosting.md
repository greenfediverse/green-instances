<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" media="all" href="style.css" />
    <title>Firebase Hosting</title>
</head>
<body>
<p><strong>Hosting Provider:</strong></p>
<a href="https://firebase.google.com/docs/hosting/" target="_blank">Firebase Hosting</a>
<p><strong>Published green energy certificate:</strong></p>
<p><strong>Critical green energy and sustainability notes:</strong></p>
<p><em>About the data center:</em></p>
Firebase Hosting is no longer an independent hosting provider, having been fully acquired by Google. It completely runs on the Google Cloud Platform (GCP). What it offers is a backend-as-a-service (BaaS) platform for mobile app developers.
It consists of a friendlier front-end to the GCP, but a Firebase project is (under the hood) a GCP project.
<a href="https://firebase.google.com/firebase-and-gcp/" target="_blank">(source)</a> Since Google has already been rejected, we can just point to the decision rationale for that. Same should apply to Firebase.
Separately from that, I am not sure Firebase offers the ability to host Fediverse software. It is sort of an app-builder or site-builder that provides various functions like a sign-in workflow or an in-website messaging feature.
<p><em>About the energy:</em></p>
<p><strong>Further research:</strong></p>
<p><strong>Other notes:</strong></p>
</body>
</html>