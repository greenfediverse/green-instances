<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <title>GreenFediverse</title>
    <link rel="stylesheet" media="all" href="style.css" />
</head>
<body>
<md-block>

**Hosting Provider: 1984 Hosting Company**

**Published green energy certificate:**

not available

**Critical green energy and sustainability notes:**

**About the data center:**

"1984 commits to environmentally responsible business practices by utilizing only electricity from renewable energy sources and placing emphasis on reaching the highest possible energy utilization efficiency levels. This is achievable in Iceland, because all electricity production meets the aforementioned criteria and the cold climate makes PUE (Power Usage Effectiveness) very high, meaning that for each watt used to run servers, storage and network equipment, proportionally very little is used for cooling, lighting and other overhead."

**About the energy:**

"Green, sustainable geothermal and hydro power energy." from Iceland.

**Further research:**

missing certificate, information about server-hardware missing, unknown location of power-plants

**Other notes:**
        
</md-block>
</body>
</html>