<p><strong>Hosting Provider:</strong> <a href="https://www.hostsharing.net/">Hostsharing</a></p>
<p><strong>Published green energy certificate:</strong></p>
<p>-not available-</p>
<p><strong>Critical green energy and sustainability notes:</strong></p>
<p><em>About energy and the data center:</em></p>
<p>Hostsharing eG offers hosting with a small ecological footprint in a friendly community <a
        href="https://www.hostsharing.net/vorteile/nachhaltigkeit-und-soziale-gemeinschaft/">(source)</a>. &quot;Hostsharing
    operates its servers in the primary data center exclusively with electricity from renewable resources. In 2020, our
    primary data center sourced 100% green electricity from renewable energy sources from Mainova AG. The product used
    (ÖkoBasis) was certified by TÜV SÜD. The requirements were tested in accordance with the VdTÜV Basic Guideline on
    Green Electricity products of the leaflet Energy 1304 version 10.2014. The primary Hostsharing&#39;s primary data
    center also has an energy management system certified to DIN EN ISO 50001. certified energy management system. Our
    servers in the secondary data center of <a href="https://www.ipb.de/colocation/">IPB Internet Provider in Berlin
        GmbH</a> are operated with <a href="https://www.enviam.de/geschaeftskunden/stromlieferung/businessoekostrom">enviaM
        BusinessÖkoStrom </a>of envia Mitteldeutsche Energie AG. The electricity is generated from 100% renewable energy
    sources and meets the criteria of &quot;CMS Standard EE01&quot; of TÜV SÜD for renewable energies&quot; You can read
    these details in their sustainability report <a
            href="https://www.hostsharing.net/downloads/nachhaltigkeitsberichte/nachhaltigkeitsbericht-2019-2020.pdf">(Nachhaltigkeitsbericht
        2020)</a></p>
<p>The company <a href="https://de.wikipedia.org/wiki/Envia_Mitteldeutsche_Energie">envia Mitteldeutsche Energie AG</a>
    using a energy source mix and they are not a 100% renewable energy supplier. In 2020, 8.9% nuclear energy and 18.6%
    coal energy were sold. See electricity labeling <a
            href="https://www.enviam.de/Media/docs/default-source/dokumente_privatkunden/strom-f%C3%BCr-zuhause/stromkennzeichnung/stromkennzeichnung_a4_2020_web.pdf?sfvrsn=aa0ed899_4">Stromkennzeichnung
        2020</a>.</p>
<p>The company <a href="https://de.wikipedia.org/wiki/Mainova">Mainova AG</a> using a energy source mix and they are not
    a 100% renewable energy supplier. 6.9% nuclear, 14,4% coal and 9,5% gas energy were sold. See electricity labeling
    <a href="https://www.mainova.de/de/stromkennzeichnung-der-mainova-strommix-im-ueberblick">Stromkennzeichnung</a>.
</p>
<p><strong>Further research:</strong></p>
<p><strong>Other notes:</strong></p>
<p>ECG/GWÖ (ecogood.org) since 2018, buy green hardware (based on oekom/imug ratings) and increased service life time,
    convincing sustainability report.</p>
