<p><strong>Cloudflare</strong></p>
<p>Cloudflare tracks whether a user is downloading images, and if not they presume to be dealing with a bot and give
    harsh treatment. These users are denied access bluntly &amp; outright, or a graphical CAPTCHA is forced on them
    (i.e. many bandwidth-wasting images), which works out as a denial if their UI can&#39;t render the images. The
    CAPTCHA is also cascading javascript, so uMatrix users must reload the page 4 times just to render the puzzle.</p>
<p>However, refusing to download image data (e. g. when using a text browser or a GUI browser with image loading
    disabled, such as visually impaired users would prefer) is actually a comparably green use of the web since
    web-served images are responsible for an immeasurable amount of energy consumption. This is because images are far
    more bulky than text and also because every image on a webpage requires a separate HTTP request to fetch it.</p>
<p>See the <a href="https://gitlab.com/crimeflare/cloudflare-tor" target="_blank">crimeflare</a> project to understand the dimensions of
    energy waste related to privacy, data trading, access inequality, net-neutrality and other aspects.</p>
