<p><strong>Hosting Provider:</strong> <a href="https://www.bit.nl" target="_blank">BIT</a></p>
<p><strong>Published green energy certificate:</strong></p>
<p><a href="https://www.bit.nl/uploads/images/PDF-Files/GroenCertificaat_NederlandseWind_Indicatief_BIT_BV_2020.pdf" target="_blank">2020</a>, <a href="https://www.bit.nl/uploads/images/PDF-Files/GroenCertificaat_NederlandseWind_Indicatief_BIT_BV_2021.pdf" target="_blank">2021</a> and <a href="https://www.bit.nl/uploads/images/PDF-Files/GroenCertificaat_NederlandseWind_Indicatief_BIT_BV_2022.pdf" target="_blank">2022</a></p>
<p><strong>Critical green energy and sustainability notes:</strong></p>
<p><em>About the data center:</em></p>
<p style="text-align: justify">
    &quot;BIT invests a lot in energy saving.
    All of BIT&#39;s data centers run on green energy,
    generated from wind. With 100% Dutch wind from Dutch windmills.
    [...] This renewable energy source has no CO2 side product.
    Power consumption is taken into account when purchasing equipment.
    This goes for our own equipment as well as in the recommendations we make to our clients.
    The air-conditioning in our data centers uses the outside air and water to keep the temperatures down.
    By using closed cold corridors, the cooled air is used efficiently.
    More and more people are choosing for electric cars.
    BIT encourages this initiative because of the positive contribution it makes to a cleaner environment.
    BIT installed charging stations to enable visitors with independent
    access to our data centers to charge their cars for free.&quot;
    <a href="https://www.bit.nl/en/about-bit/corporate-social-responsibility" target="_blank">(source)</a></p>
<p><em>About the energy:</em></p>
<p style="text-align: justify">
    BIT uses green electricity certificates and guarantees of origin by
    <a href="https://en.wikipedia.org/wiki/Vattenfall_Nederland" target="_blank">Vattenfall Energy Trading Netherlands N.V.</a>
which belongs to the Swedish multinational power company Vattenfall owned by the Swedish State.
    See <a href="https://en.wikipedia.org/wiki/Vattenfall#Criticism" target="_blank">Criticism on Wikipedia</a></p>

<p><strong>Further research:</strong></p>
<p><strong>Other notes:</strong></p>