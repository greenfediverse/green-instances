<p><strong>Amazon</strong></p>
<p>Amazon AWS datacenters are 50% clean energy. However, Amazon as a whole is extremely detrimental to the environment
    for several reasons:</p>
<ul>
    <li>Prices in Amazon&#39;s stores are extremely competitive. This is terrible not just because it kills small
        business but because it encourages mass consumption. People are buying huge amounts of goods they don&#39;t
        need, and this is absolutely a big part of environmental destruction.
    </li>
    <li>Amazon&#39;s excessive packaging <a
            href="https://www.forbes.com/sites/jonbird1/2018/07/29/what-a-waste-online-retails-big-packaging-problem">destroys
        1 billion trees annually</a> (more <a
            href="https://www.buzzfeed.com/morenikeadebayo/amazon-packaging-needs-to-chill-the-fuck-out">examples</a>).
    </li>
    <li>Amazon <a href="https://www.tbray.org/ongoing/When/202x/2020/04/29/Leaving-Amazon">retaliates against</a>
        employees who seek climate action.
    </li>
    <li>Amazon works for BP and Shell to deliver a <a href="http://qklhadlycap4cnod.onion/watch?v=v3n8txX3144">machine
        learning service</a> to discover locations to drill for oil and gas.
    </li>
    <li>Amazon has <a
            href="https://www.theguardian.com/environment/2019/oct/11/google-contributions-climate-change-deniers">been
        caught</a> financing climate deniers.
    </li>
</ul>
<p>Amazon trashes large quantities of new undamaged and unopened products (phones, headsets, food, etc). When a vendor
    doesn&#39;t accept product back Amazon falsely scans it in their system as &quot;damaged&quot;. If it&#39;s food
    waste it&#39;s rationalised as &quot;food contains allergens&quot;. If that&#39;s a problem for Amazon then why did
    they opt to sell peanuts or whatever in the first place? Obviously they&#39;re fabricating the truth which seems
    like fraud. It&#39;s unclear if the consequence of falsely treating undamaged goods as damaged leads to unlawful
    accounting.
    Generally the waste happens because the warehouse has a finite space and the input exceeds the output. Products that
    don&#39;t move as fast apparently don&#39;t make the cut for precious warehouse space.
    Amazon blocks employees from taking the &quot;damaged&quot; products home (as they don&#39;t want the stuff ending
    up on eBay). Amazon goes to great lengths to control who handles the products, essentially ensuring that the
    products don&#39;t go good use (as that would compete with their store). If they were to donate the products to
    charity, Amazon likely fears losing a potential sale from the recipient. Only specific employees are deemed
    trustworthy enough to work in the part of the warehouse that scraps perfectly good products.
    It&#39;s a despicable waste and it&#39;s an attack on the environment.</p>
<p>&quot;the company remains the least transparent among the big three cloud companies, failing to provide even basic
    information about its energy demand.&quot; &quot;Amazon Web Services is the largest cloud service provider in the
    world, with an ever growing network of data centers, often situated in locations with dirty forms of energy.&quot;
    &quot;It gets worse – Amazon has threatened to fire employees for speaking out about the company’s climate policy.&quot;
    <a href="https://www.greenpeace.org/usa/microsoft-google-amazon-energy-oil-ai-climate-hypocrite/">source</a></p>
