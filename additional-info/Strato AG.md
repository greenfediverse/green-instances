<strong>Hosting Provider: <a href="https://www.strato.de/" target="_blank">Strato
AG</a> belongs to <a href="https://de.wikipedia.org/wiki/United_Internet" target="_blank">United
Internet</a></strong></p>
<p><strong>Published green energy certificate:</strong></p>
<p><strong><span style="font-weight: normal">unknown</span></strong></p>
<p><strong>Critical green energy and sustainability notes:</strong></p>
<p><strong><span style="font-weight: normal"><a href="https://www.strato.de/ueber-uns/nachhaltigkeit/" target="_blank">Here</a>
you can find general information by Strato AG. </span></strong><strong><span style="font-weight: normal">Strato
AG belongs to United Internet. The <a href="https://www.united-internet.de/investor-relations/aktie/daten-fakten.html" target="_blank">ownership
structure </a>of United Internet lists BlackRock with 3%, those with
more than 3% hold voting rights.</span></strong></p>
<p><em>&bdquo;STRATO ist klimaneutral: Seit 2008 bezieht STRATO f&uuml;r
seine Rechenzentren bereits ausschlie&szlig;lich Strom aus
erneuerbaren Energien beim Stromanbieter Naturstrom AG &ndash; und
geht nun den n&auml;chsten Schritt: Als Vorreiter seiner Branche
agiert STRATO 100 % klimaneutral, unabh&auml;ngig best&auml;tigt von
seinem Partner South Pole. J&auml;hrliche CO&#8322;-Messung aller
Dienstleistungen, Liefer und Arbeitswege. CO&#8322;-Kompensation
durch Unterst&uuml;tzung verifizierter Umweltprojekte. J&auml;hrlich
weitere Reduzierung der CO&#8322;-Emissionen.&ldquo;</em></p>
<p><strong><span style="font-weight: normal">Strato AG servers are
using 100% real green energy by <a href="https://www.naturstrom.de/ueber-uns" target="_blank">Naturstrom
AG</a> &ndash; but there is no published energy certificate
available. <a href="https://www.robinwood.de/oekostromreport/naturstrom" target="_blank">Here</a>
you can read how the NGO Robin Wood rated Naturstrom </span></strong><strong><span style="font-weight: normal">and
how it looks in detail</span></strong><strong><span style="font-weight: normal">.
</span></strong><strong><i><span style="font-weight: normal">&bdquo;In
2018, 100 % of the electricity offered by NATURSTROM consisted of
hydropower. &bdquo;NATURSTROM obtains its electricity entirely
through direct supply contracts with green power plants in Germany
and Austria.&ldquo;</span></i></strong><strong><span style="font-weight: normal">
Every customer supports the energy turnaround with an amount (1-2
ct/kWh).</span></strong></p>
<p><em>&bdquo;<i>Ermittelt und kompensiert wird der CO&#8322;-Fu&szlig;abdruck
von STRATO nach international anerkannten Standards in Zusammenarbeit
mit South Pole, einem f&uuml;hrenden Anbieter von
Klimaschutzl&ouml;sungen. In die Berechnungen flie&szlig;en alle
direkten und indirekten Emissionen ein.&ldquo; </i></em><em><span style="font-style: normal">This
means, </span></em><strong><span style="font-weight: normal">Strato
calculate their CO&#8322; footprint with <a href="https://www.southpole.com/" target="_blank">South
Pole</a> and use its CO2 offsets </span></strong><strong><span style="font-weight: normal">for
compensation</span></strong><strong><span style="font-weight: normal">.
</span></strong>
</p>
<p><strong><span style="font-weight: normal">Co2-Offset projects are:
Restoration of K&ouml;nigsmoor in Schleswig-Holstein by <a href="https://moorfutures-schleswig-holstein.de/Koenigsmoor" target="_blank">MoorFutures</a>
(<a href="https://moorfutures-schleswig-holstein.de/WebRoot/Store27/Shops/213cd9a1-419b-4d96-b3c9-1d69f463a357/MediaGallery///Monitoring-Bericht_2020.pdf" target="_blank">Last
report</a>) which belongs to <a href="https://www.ausgleichsagentur.de/" target="_blank">Ausgleichsagentur
Schleswig-Holstein GmbH</a> (<a href="https://www.ausgleichsagentur.de/fileadmin/user_upload/AUGL_pdf/MF_Projektdokument_Koenigsmoor_2.0.pdf" target="_blank">report
about the project</a>). <a href="https://www.ausgleichsagentur.de/moorfuturesr/projektgebiet-koenigsmoor/" target="_blank">Here</a>,
you will find more information about this project. Ausgleichsagentur
belongs to 100% to <a href="https://de.wikipedia.org/wiki/Stiftung_Naturschutz_Schleswig-Holstein" target="_blank">Stiftung
Naturschutz Schleswig-Holstein</a> which is a Foundation under public
law. Information about the second project &bdquo;Wind power in New
Caledonia&ldquo; are published <a href="https://www.southpole.com/de/projekte/prony-windfarms-improving-lives-of-local-communities" target="_blank">here</a>.</span></strong></p>
<p><a name="firstHeading"></a><strong><span style="font-style: normal"><span style="font-weight: normal">The
</span></span></strong><em><span style="font-style: normal"><span style="font-weight: normal"><a href="https://en.wikipedia.org/wiki/South_Pole_Group" target="_blank">South
Pole Group</a> is a company that is growing fast and buying up
competitors in its sector.</span></span></em><em><span style="font-weight: normal">
&bdquo;The company originally started as a spin-off from the <a href="https://en.wikipedia.org/wiki/ETH_Zurich" target="_blank">ETH
Zurich</a> as the non-profit, myclimate.&ldquo; </span></em><em><span style="font-style: normal"><span style="font-weight: normal">Today,
South Pole is increasingly subject to the mechanisms of the
capitalist-driven market. </span></span></em><em><span style="font-style: normal"><span style="font-weight: normal">E.g.
t</span></span></em><strong><span style="font-style: normal"><span style="font-weight: normal">he
company <a href="https://www.lightrock.com/about" target="_blank">Lightrock</a>
owns </span></span></strong><strong><span style="font-style: normal"><span style="font-weight: normal">in
a deal </span></span></strong><strong><span style="font-style: normal"><span style="font-weight: normal">10%
of the shares, which aims to generate financial as well as social and
environmental returns through impact investing. Lightrock belongs to
LGT. The </span></span></strong><strong><i><span style="font-weight: normal">&bdquo;<a href="https://en.wikipedia.org/wiki/LGT_Group" target="_blank">LGT
Group</a> is the largest family-owned private banking and asset
management group in the world&ldquo;</span></i></strong><strong><span style="font-weight: normal">.
&bdquo;LGT, the international private banking and asset management
group owned by the <a href="https://en.wikipedia.org/wiki/House_of_Liechtenstein" target="_blank">Princely
House of Liechtenstein</a>&ldquo;. Take a look at the controvery
<a href="https://en.wikipedia.org/wiki/2008_Liechtenstein_tax_affair" target="_blank">Liechtenstein
tax affair</a>.</span></strong></p>
<p><em><span style="font-weight: normal">Strato: &bdquo;In den
kommenden Jahren wollen wir die Anspr&uuml;che an uns selbst weiter
erh&ouml;hen. Wir haben uns verpflichtet, unseren CO&#8322;-Aussto&szlig;</span></em><em>
in den kommenden drei Jahren um insgesamt zw&ouml;lf Prozent zu
senken. Schlie&szlig;lich ist es noch besser, Kohlendioxid zu
vermeiden, als es hinterher zu kompensieren.&ldquo;</em></p>
<p><strong>Further research:</strong></p>
<p><strong><span style="font-weight: normal">Not transparent: How
many MoorFutures did Strato AG buy? &bdquo;Das von uns unterst&uuml;tzte
Projekt renaturiert das Moor und spart so pro Jahr umgerechnet mehr
als 1.000 Tonnen CO&#8322;.&bdquo; means: 1000 MoorFutures? 1
MoorFuture is 1 Tonne Co&sup2; = 64&euro;, 64&euro; x 1000 tons =
64000&euro;?</span></strong></p>
<p><strong>Other notes:</strong></p>
</body>
</html>