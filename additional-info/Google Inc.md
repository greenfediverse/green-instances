<!DOCTYPE html>

<html>
<head>
    <meta charset='utf-8' />
    <link rel="stylesheet" media="all" href="style.css" />
</head>
<body>

<p><strong>Hosting Provider:</strong></p>

  <a href="https://cloud.google.com/">Google</a>

<p><strong>Energy certificate:</strong></p>

Certificate

<p><strong>About the datacenter:</strong></p>

  "Google Cloud net operational greenhouse gas (GHG) emissions: After calculating our Scope 2 market-based emissions per the GHG Protocol including our renewable energy contracts, Google ensures any remaining Scope 2 emissions are neutralized by investments in carbon offsets; this brings our global net operational emissions to zero."

https://cloud.google.com/sustainability/region-carbon?hl=de

<p><strong>Colocation:</strong></p>

...text...

<p><strong>About renewable energy:</strong></p>

...text...

<p><strong>Other notes:</strong></p>
  About the measurement "24/7 CFE":
  "24/7 CFE has already been adopted by climate leaders such as Google, Iron Mountain, Mercedes-Benz, and Microsoft, while 24/7 CFE tariffs are offered by international energy suppliers, such as Engie, Vattenfall, AES, and Quinbrook, etc."
  

<p>Google spent $35,178 to co-sponsor a conference which featured groups that promote climate change denial. Google itself is helping fossil fuel companies utilise a wide range of its technologies to get oil and gas out of the ground, which will, undeniably, accelerate the process of climate change.</p>
<p>In 2018, Google started an oil, gas, and energy division.</p>
<p><strong>sources:</strong></p>
<p><a href="https://www.gizmodo.com.au/2019/02/how-google-microsoft-and-big-tech-are-automating-the-climate-crisis/">https://www.gizmodo.com.au/2019/02/how-google-microsoft-and-big-tech-are-automating-the-climate-crisis/</a>
<p><a href="https://www.greenpeace.org/usa/microsoft-google-amazon-energy-oil-ai-climate-hypocrite/">https://www.greenpeace.org/usa/microsoft-google-amazon-energy-oil-ai-climate-hypocrite/</a>
</p>
