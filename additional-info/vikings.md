<p><strong>Vikings</strong></p>
<p>Vikings GmbH</p>
<p>are powered by certified green energy usage of refurbished hardware.
    Workstations and servers that respect your freedom and privacy.
    Based on libre boot firmware and libre GNU/Linux distros!</p>
<p><a href="http://vikings.net">http://vikings.net</a></p>
<p>also on Mastodon:
    <a href="https://social.vikings.net/@vikings">https://social.vikings.net/@vikings</a></p>
