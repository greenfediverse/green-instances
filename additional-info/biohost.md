<p><strong>Hosting Provider:</strong> <a href="https://www.biohost.de" target="_blank">Biohost</a></p>
<p><strong>Published green energy certificate:</strong></p>
<p>
    <a href="https://www.verbund.com/-/media/verbund/ueber-verbund/verantwortung/umwelt/umweltzertifizierungen/hydropower-innkraftwerk-de-certificate-iso14001-en.ashx?ori=1&amp;la=en" target="_blank">Link
        to certificate (pdf)</a></p>
<p><strong>Critical green energy and sustainability notes:</strong></p>
<p><em>About the data center:</em></p>
        &quot;In many businesses, servers are retired after 3-5 years and replaced with new equipment. BIOHOST gives leasing
    returns a second life cycle. In this way, we significantly reduce the material consumption of precious raw
    materials, without you having to suffer any disadvantages in your web hosting. For air conditioning, we rely on
    direct free cooling, i.e. we use filtered outside air to cool the valuable IT equipment. However, this is only
    possible up to temperatures of max. 25 °C. For some weeks of the year, this is not possible, at least during the
    day, and the air must be cooled artificially using an air conditioning system. With free cooling, we ensure enormous
    energy savings.&quot; <a href="https://www.biohost.de/ueber-uns/" target="_blank">(source)</a>
<p><em>About the energy:</em></p>
    <p style="text-align: justify">
    Biohost uses energy by <a href="https://www.polarstern-energie.de/" target="_blank">Polarstern</a>, which purchases energy by <a
        href="https://de.wikipedia.org/wiki/Verbund_AG" target="_blank">Verbund AG</a></p>
    <p style="text-align: justify">
        For the generation of truly green electricity, we cooperate with two <a
        href="https://de.wikipedia.org/wiki/Inn#Elektrizit%C3%A4tswerke" target="_blank">Inn power plants</a> - near <a
        href="https://de.wikipedia.org/wiki/Kraftwerk_Rosenheim" target="_blank">Rosenheim</a> and in <a
        href="https://de.wikipedia.org/wiki/Kraftwerk_Wasserburg" target="_blank">Wasserburg</a>.&quot; They are operated by Verbund AG
    - here they take care of
    <a href="https://www.verbund.com/en-at/about-verbund/power-plants/our-power-plants/rosenheim" target="_blank">migration
        assistance for fish and renaturation</a> measures. Verbund AG is Austria&#39;s largest electricity supply
    company. They cover &quot;over 40 percent of Austria&#39;s electricity needs and generates 90 percent of its output
    from hydropower.&quot; Verbund AG also trades in green electricity and CO2 certificates. In the past the &quot;ambiguous
    communication&quot; is criticized, as Verbund AG advertises with electricity from clean hydropower and selled
    imported electricity that contains 30 % nuclear power&quot; - for this critic there is no
    <a href="https://de.wikipedia.org/wiki/Verbund_AG#Kritik" target="_blank">source</a> available anymore, but a similar case
    today is that the wholly owned subsidiary &quot;Verbund Energy4Business GmbH&quot; sells electricity from fossil
    energy sources, see:
    <a href="https://www.e-control.at/home_de?p_p_id=com_liferay_portal_search_web_portlet_SearchPortlet&amp;p_p_lifecycle=0&amp;p_p_state=maximized&amp;p_p_mode=view&amp;_com_liferay_portal_search_web_portlet_SearchPortlet_mvcPath=%2Fview_content.jsp&amp;_com_liferay_portal_search_web_portlet_SearchPortlet_redirect=https%3A%2F%2Fwww.e-control.at%2Fhome_de%3Fp_p_id%3Dcom_liferay_portal_search_web_portlet_SearchPortlet%26p_p_lifecycle%3D0%26p_p_state%3Dmaximized%26p_p_mode%3Dview%26_com_liferay_portal_search_web_portlet_SearchPortlet_cur%3D1%26_com_liferay_portal_search_web_portlet_SearchPortlet_mvcPath%3D%252Fsearch.jsp%26_com_liferay_portal_search_web_portlet_SearchPortlet_keywords%3D%2522verbund%2Bag%2522%26_com_liferay_portal_search_web_portlet_SearchPortlet_entryClassName%3D%26_com_liferay_portal_search_web_portlet_SearchPortlet_formDate%3D1642547643086%26_com_liferay_portal_search_web_portlet_SearchPortlet_searchPrimaryKeys%3D%26_com_liferay_portal_search_web_portlet_SearchPortlet_groupId%3D1785851%26_com_liferay_portal_search_web_portlet_SearchPortlet_format%3D%26_com_liferay_portal_search_web_portlet_SearchPortlet_userName%3D%26_com_liferay_portal_search_web_portlet_SearchPortlet_modifiedfrom%3D18.01.2022%26_com_liferay_portal_search_web_portlet_SearchPortlet_folderId%3D%26_com_liferay_portal_search_web_portlet_SearchPortlet_assetTagNames.raw%3D%26_com_liferay_portal_search_web_portlet_SearchPortlet_assetCategoryIds%3D%26_com_liferay_portal_search_web_portlet_SearchPortlet_scope%3Dthis-site%26_com_liferay_portal_search_web_portlet_SearchPortlet_modifiedselection%3D5%26_com_liferay_portal_search_web_portlet_SearchPortlet_modified%3D%255B20210119001344%2BTO%2B20220119001344%255D%26_com_liferay_portal_search_web_portlet_SearchPortlet_useAdvancedSearchSyntax%3Dfalse%26_com_liferay_portal_search_web_portlet_SearchPortlet_modifiedto%3D19.01.2022&amp;_com_liferay_portal_search_web_portlet_SearchPortlet_assetEntryId=10316984&amp;_com_liferay_portal_search_web_portlet_SearchPortlet_type=document" target="_blank">E-Control:
        Stromkennzeichnungsbericht 2021 - page 12</a> by
    <a href="https://de.wikipedia.org/wiki/Energie-Control_Austria_f%C3%BCr_die_Regulierung_der_Elektrizit%C3%A4ts-_und_Erdgaswirtschaft" target="_blank">e-control</a>.
</p>
<p>A further reading and critical view on hydro-power in austria:
    <a href="https://www.umweltdachverband.at/themen/wasser/wasserkraft/" target="_blank">&quot;Wasserkraft: Flüsse im Spannungsfeld
    der Interessen&quot;</a>.</p>
<p><strong>Further research:</strong></p>
<p>It should be checked whether Verbund AG-investments in new hydropower plants are planned in ecologically critical
    areas - names of new projects can be found in the Verbund AG overview on page 72
    <a href="https://www.verbund.com/-/media/verbund/ueber-verbund/investor-relations/finanzpublikationen/de/2021/verbund-integrierter-geschaeftsbericht-2020-optimierte-barrierefreiheit.ashx" target="_blank">Geschäftsbericht
        (pdf)</a>.</p>
<p><strong>Other notes:</strong></p>